<?php
/**
 * Display the Home Page of website if have any set in Reading Settings => Static Page
 * Else show all posts.
 * For Blog use home.php and set it in reading Settings too.
 */
get_header();?>

<div class="container">
    <main id="main" class="content-area primary" role="main">

	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post(); ?>
			<div class="content">
<!--                <h2>--><?php //the_title(); ?><!--</h2>-->
                <?php the_content(); ?>
            </div>
		<?php
        endwhile;

	endif;?>

    </main>

    <?php
    // Sidebar.
    //get_sidebar(); // Supprimer la sidebar donne toute la largeur au contenu principal .primary
    ?>
</div>

<?php
// Afficher le footer.
get_footer();
