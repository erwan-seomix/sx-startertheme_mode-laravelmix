<?php
/**
 * Display the Home Page for Blog if have any set in Reading Settings => Static Page
 */
get_header();?>

<div class="container">
    <main id="main" class="content-area primary" role="main">

        <?php
        if ( have_posts() ) :
            while ( have_posts() ) :
                the_post();
	            get_template_part( 'loop', 'listing' );
            endwhile;

        endif;
        ?>

    </main>

	<?php
	get_sidebar();?>
</div>

<?php
get_footer();
