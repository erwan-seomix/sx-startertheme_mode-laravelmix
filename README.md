Test de starter thème  

Nécessite NODE.js dévelopé en Node.14.13.1
- télécharger le thème 
- "cd SX-new_Starter_theme" 
- "npm install" 
- copier local-example.json et le renommer en local.json enadaptant l'url du site en local
- "npm run dev" pour lancer le watch et commencer a Développer
- Une fois le Dev terminé "npm run build" pour générer un dossier build à uploader sur le thème en prod.

## 1- Mode développement 
-On dev en mode "npm run dev" 
	Gros avantage c'est que dès lors que ça génère un localhost:3000 qui permet de voir les modifs Js et Css en live sur le navigateur sans avoir rien a recharger.
-pas beaucuoup de changement pour le développement du thème,
	* project-content-enhanced fonctionne de la même manière, j'ai juste adapté le chemin qui mène désormais à build et plus a assets. 
	* SEUL changement c'est en javascript . pour que les scripts soient bien appelés, ça fonctionne un peu comme  pour SASS. 
		app.js dans les sources (!! Attention il s'appelle assets pas src ici !!) accueille tous les appels des différents fichiers js et compilera vers "build/js/apps.js"

## 2- Mode Production
-Pour  lancer en prod on fait un "npm run prod", ça génère un dossier build que tu peux uploader sur le thème dans la prod . 


ps: plusieurs petites choses a apprendre : 
	* Essayer de gérer webpack avec  un accès ssh sur site de dev.
	* Implémenter la grstion des icones comme sur le theme initial
	* Tester la mise en prod avec le dossier build

----
	
	Avec ça on a qqch de bien moderne en dev local et avec une ou deux heures je devrai arriver a gérer le ssh et une gestion en direct sur le serveur de dev

