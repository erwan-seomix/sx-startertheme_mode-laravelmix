<?php
/**
* Add an anchor to form to deal with anchor scrolling when menu is fixed
* https://docs.gravityforms.com/gform_confirmation_anchor/
* Add somme css to .gform_anchor to add margin
*/
//add_filter( 'gform_confirmation_anchor', '__return_true' );

/**
 * Validation du format du téléphone
 */
//add_filter( 'gform_field_validation', 'sxstarter_validate_phone', 10, 4 );
function sxstarter_validate_phone( $result, $value, $form, $field ) {
	$pattern = "/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/";
	if ( ! empty( $value ) ) {
		if ( $field->type == 'phone' && $field->phoneFormat == 'international' && ! preg_match( $pattern, $value ) ) {
			$result['is_valid'] = false;
			$result['message']  = 'Renseignez un numéro de téléphone valide';
		}
	}

	return $result;
}