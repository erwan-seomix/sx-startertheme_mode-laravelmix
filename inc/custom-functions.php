<?php
/**
 * Additionnals functions : hook filter and other necessary
 */

/**
 * Function wp_body_open Backward Compatibility.
 */
if ( ! function_exists( 'wp_body_open' ) ) {
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}

/**
 * Head Data : Add basic HTLM information in <head>
 *
 * @echo string HTML Data
 */
add_action( 'wp_head', 'sxstarter_head_content_base', 1 );
function sxstarter_head_content_base() {
	$render  = '<meta charset="UTF-8">' . "\n";
	$render .= '<meta name="viewport" content="width=device-width, initial-scale=1.0">' . "\n";
	$render .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">' . "\n";
	$render .= '<meta name="HandheldFriendly" content="true" >' . "\n";
	echo $render;
}

/**
 * Favicons
 *
 * http://realfavicongenerator.net/
 *
 * @since  1.0
 * @author  Daniel Roch
 *
 * @hook wp_head, 20
 */
//add_action( 'wp_head', 'sxstarter_head_content_favicons', 20 );
//function sxstarter_head_content_favicons() {
//
//	$path = get_template_directory_uri() . '/assets/img';
//
//	$render = '<link rel="apple-touch-icon" sizes="180x180" href="' . $path . '/apple-touch-icon.png">
//	<link rel="icon" type="image/png" sizes="32x32" href="' . $path . '/favicon-32x32.png">
//	<link rel="icon" type="image/png" sizes="16x16" href="' . $path . '/favicon-16x16.png">
//	<link rel="mask-icon" href="' . $path . '/safari-pinned-tab.svg" color="#5bbad5">
//	<meta name="msapplication-TileColor" content="#da532c">
//	<meta name="theme-color" content="#ffffff">';
//	echo $render;
//}

/**
 * Nettoyage / Ajout des classes au menu
 * © Willy Bahuaud  https://bitbucket.org/willybahuaud/hooks-menus-wordpress/src/0a28300dc23f?at=master
 */
function sxstarter_menu_item_classes( $classes, $item, $args ) {
//	// Gardons seulement les classes qui nous intéressent => on garde tout ! utile !!!
//	$classes = array_intersect( $classes, array(
//		'menu-item',
//		'current-menu-item',
//		'current-menu-parent',
//		'current-menu-ancestor',
//		'menu-item-has-children',
//	) );
	// Ajoutons une classe pour désigner les éléments vides.
	if ( "#" == $item->url ) {
		$classes[] = 'empty-item';
	}
	return $classes;
}
add_action( 'nav_menu_css_class', 'sxstarter_menu_item_classes', 10, 3 );

/**
 * Changer les liens vides en span dans les menus
 * © Willy Bahuaud  https://bitbucket.org/willybahuaud/hooks-menus-wordpress/src/0a28300dc23f?at=master
 */
add_filter( 'walker_nav_menu_start_el', 'sxstarter_empty_nav_links_to_span', 10, 4 );
function sxstarter_empty_nav_links_to_span( $item_output, $item, $depth, $args ) {
	if ( '#' == $item->url || true == $item->current ) {
		$item_output = preg_replace( '/<a.*?>(.*)<\/a>/', '<span>$1</span>', $item_output );
	}
	return $item_output;
}

/**
 * Système de pagination
 */
if ( ! function_exists( 'sxstarter_pagination' ) ) {
	function sxstarter_pagination( $custom_query = null ) {

		if ( ! empty( $custom_query ) ) {
			$wp_query = $custom_query;
		} else {
			global $wp_query;
		}

		global $wp_rewrite;
		$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

		$pagination = array(
			'base'      => @add_query_arg( 'page', ' %#%' ),
			'format'    => '',
			'total'     => $wp_query->max_num_pages,
			'current'   => $current,
			'show_all'  => false,
			'end_size'  => 1,
			'mid_size'  => 2,
			'type'      => 'list',
			'next_text' => '→',
			'prev_text' => '←',
		);

		if ( $wp_rewrite->using_permalinks() ) {
			$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
		}

		if ( ! empty( $wp_query->query_vars['s'] ) ) {
			$pagination['add_args'] = array( 's' => str_replace( ' ', '+', get_query_var( 's' ) ) );
		}

		$pagination = str_replace( 'page/1/', '', paginate_links( $pagination ) );

		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$html = preg_replace( '#<li(.*?)><a class=\'page-numbers(.*?)>(.*?)</a></li>#', '', $pagination, 1 );
		echo $html;
	}
}
function sxstarter_pagination_callback( $m ) {
	return ' href="' . $m[1] . '">' . $m[2] . '</a>';
}


/**
 *  Activation des shortcodes dans les widgets texte
 */
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Fixer le nombre d'articles par page sur les pages auteur
 */
function sxstarter_author_articles( $query ) {
	if ( $query->is_author() && $query->is_main_query() ) :
		$query->set( 'posts_per_page', 10 );
		return;
	endif;
}
add_action( 'pre_get_posts', 'sxstarter_author_articles' );

/**
 * LOGIN Changer la balise href du logo
 */
function sxstarter_login_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'sxstarter_login_url' );

/**
 * Ajouter un message d'accueil au login
 */
function sxstarter_login_custom_login_message( $message ) {
	if ( empty( $message ) ) {
		return '<p class="paccueil">Bienvenue sur <span class="colored">' . get_option( 'blogname' ) . '</span>.</p><p class="accueil">Connectez-vous au site !</p><br><p>Site créé par <a href="https://www.seomix.fr/">SeoMix</a></p><br>';
	} else
		return $message;
}
add_filter( 'login_message', 'sxstarter_login_custom_login_message' );

/**
 * Date de publication avec Human Time Diff
 */
function sxstarter_comments_human_time_diff_enhanced( $duration = 60 ) {
	$post_time  = get_comment_date( 'U' );
	$human_time = '';
	$time_now   = date( 'U' );
	$duration   = 2;
	// use human time if less that $duration days ago
	// 60 seconds * 60 minutes * 24 hours * $duration days
	if ( $post_time > $time_now - ( 60 * 60 * 24 * $duration ) ) {
		$human_time = sprintf( _e( 'il y a %s' ), human_time_diff( $post_time, current_time( 'timestamp' ) ) );
	} else {
		$human_time = 'le ' . get_comment_date( 'd M Y' );
	}
	return $human_time;
}

/**
 * Dates de copyright dynamiques
 * © Daniel Roch
 *  */
function sxstarter_footer_copyright_date() {
	// Je regarde d'abord si j'ai mis la date de copyright en cache.
	$nomtransient = 'seomix_transient_copyrightdate';
	$expiration   = DAY_IN_SECONDS;
	// Le transient est-il inexistant ou expiré ?
	// if ( false === ( $transient = get_transient( $nomtransient ) ) ) {
	// Quelle est la date du contenu le plus ancien ?
	global $wpdb;
	$copyright_dates = $wpdb->get_results( "
			SELECT
				YEAR(min(post_date_gmt)) AS firstdate
			FROM $wpdb->posts
			WHERE post_status = 'publish'
		" );
	$output = '';
	$lastdate = date('Y');
	if ( $copyright_dates ) {
		$copyright = "&copy; " . $copyright_dates[0]->firstdate;
		if ( $copyright_dates[0]->firstdate != $lastdate ) {
			$copyright .= '-' . $lastdate;
		}
		$output = $copyright;
	}
	// Je met à jour la valeur du transient avec $output.
	set_transient( $nomtransient, $output, $expiration );
	// }
	echo apply_filters( 'sxstarter_footer_copyright_date_filter', get_transient( $nomtransient ) );
}

/**
 * Contenu pour page d'erreur 404
 * Par Daniel roch
 */
function sxstarter_content_404suggest() {
	global $wp_query;
	$search = $wp_query->query_vars['name'];
	$search = preg_replace( "/(.*)-(html|htm|php|asp|aspx)$/", "$1", $search );
	$search = str_replace( "-", " ", $search );
	if ( !empty( $search ) ) {
		$search_term_q = preg_replace( '/ /', '%20', $search );
		query_posts( 'posts_per_page=6&s=' . $search_term_q );
	}
}


/**
 * LOGIN Changer la balise title du logo
 */
function sxstarter_login_title() {
	return get_option( 'blogname' );
}
add_filter( 'login_headertext', 'sxstarter_login_title' );


/**
 * Retirer les préfixes sur les pages d'archives
 */
add_filter(
	'get_the_archive_title',
	function ( $title ) {
		if ( is_category() || is_tax() ) {
			$title = single_cat_title( '', false );
		} elseif ( is_tag() ) {
			$title = single_tag_title( '', false );
		} elseif ( is_author() ) {
			$title = '<span class="vcard">' . get_the_author() . '</span>';
		} elseif ( is_post_type_archive() ) {
			$title = post_type_archive_title();
		} return $title;
	}
);


/**
 * Add class to links if it's external or internal
 *
 * @param [type] $content
 * @return string|string[]
 */
function sxstarter_add_class_to_links( $content ) {
	$content = preg_replace_callback(
		'/]*href=["|\']([^"|\']*)["|\'][^>]*>([^<]*)<\/a>/i',
		function( $m ) {
			$url = home_url();
			if ( strpos( $m[1], $url ) === false ) {
				return ' href="' . $m[1] . '" class="external-link">' . $m[2] . '</a>';
			} else {
				return ' href="' . $m[1] . '" class="internal-link">' . $m[2] . '</a>';
			}
		},
		$content
	);
	return $content;
}
add_filter( 'the_content', 'sxstarter_add_class_to_links' );

/**
 * Add a special class to the excerpt's p element
 * @param $excerpt
 * @return string|string[]
 */
function sxstarter_add_class_to_excerpt( $excerpt ) {
	return str_replace( '<p', '<p class="excerpt"', $excerpt );
}
add_filter( 'the_excerpt', 'sxstarter_add_class_to_excerpt' );


/**
 * Tell WordPress to remove the default more which looks like this: […], and replace it with nothing.
 * @param $more
 * @return string
 */
function sxstarter_new_excerpt_more( $more ) {
	global $post;
	return '';
}
add_filter( 'excerpt_more', 'sxstarter_new_excerpt_more' );


/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function sxstarter_excerpt_more( $more ) {
	if ( is_admin() ) {
		return $more;
	}
	return ' ...';
}
add_filter( 'excerpt_more', 'sxstarter_excerpt_more' );

//function sxstarter_excerpt_more( $more ) {
//	return sprintf( '<a href="%1$s" class="more-link">%2$s</a>',
//		esc_url( get_permalink( get_the_ID() ) ),
//		sprintf( __( 'Continue reading %s', 'wpdocs' ), '<span class="screen-reader-text">' . get_the_title( get_the_ID() ) . '</span>' )
//	);
//}
//add_filter( 'excerpt_more', 'sxstarter_excerpt_more' );

/**
 * Filter except length to 30 words.
 * @param $length
 * @return int
 */
function sxstarter_custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'sxstarter_custom_excerpt_length', 999 );


/**
 * Limit search results to posts
 */
function sxstarter_search_filter($query) {
	if ($query->is_search)
		$query->set('post_type',array('post'));
}
add_filter('pre_get_posts','sxstarter_search_filter');

/**
 * Stay on search page (blog page) if search submit is empty
 */
function sxstarter_search_redirect($query) {
	if (!is_admin() && $query->is_main_query() && empty($_GET['s'])) {
		if ($query->is_search) {
			wp_redirect(get_permalink(get_option('page_for_posts')));
			exit;
		}
	}
}
add_action('pre_get_posts','sxstarter_search_redirect');


/**
 * Add Custom styles to WYSISWYG
 */
// Add dropdown menu button
function sxstarter_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'sxstarter_mce_buttons_2');

// Callback function to filter the MCE settings
function sxstarter_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

	$style_formats = array (
		/*
		* Each array child is a format with it's own settings
		* Notice that each array has title, block, classes, and wrapper arguments
		* Title is the label which will be visible in Formats menu
		* Block defines whether it is a span, div, selector, or inline style
		* Classes allows you to define CSS classes
		* Wrapper whether or not to add a new block-level element around any selected elements
		*/
		array(
			'title' => 'Highlight content',
			'block' => 'span',
			'classes' => 'hightlight-content',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
add_filter( 'tiny_mce_before_init', 'sxstarter_mce_before_init_insert_formats' );


/*****************************************************************
Commentaires
 *****************************************************************/
/**
 * Commentaire sans le champs URL (1/2)
 *
 * @since  1.0
 * @author  Daniel Roch
 *
 * @hook comment_form_field_url
 */
function sxstarter_commenturl_removefield( $url_field ) {
	return '';
}
add_filter( 'comment_form_field_url', 'sxstarter_commenturl_removefield' );

/**
 * Commentaire sans le champs URL (2/2)
 *
 * @since  1.0
 * @author  Daniel Roch
 *
 * @hook preprocess_comment
 */
function sxstarter_commenturl_protect( $commentdata ) {
	if ( !empty( $_POST['url'] ) ) {
		wp_die( __( 'You should not be able to submit URLs. Nice try  ;)', 'sxstarter' ) );
	}
	return $commentdata;
}
add_action( 'preprocess_comment', 'sxstarter_commenturl_protect' );

/*****************************************************************
Contenus
 *****************************************************************/

/**
 * Correction du contenu HTML si nécessaire
 * © Daniel Roch
 */
function sxstarter_content_filter( $content ) {
	$content = str_replace( 'style="text-align: left;"', '', $content );
	$content = str_replace( 'style="text-align: left"', '', $content );
	$content = str_replace( 'style="text-align:left;"', '', $content );
	$content = str_replace( 'style="text-align:left"', '', $content );
	return $content;
}
//add_filter( "the_content", "sxstarter_content_filter" );

/**
 * Add Bootstrap thumbnail styling to images with captions
 * Use <figure> and <figcaption>
 *
 * @link http://justintadlock.com/archives/2011/07/01/captions-in-wordpress
 * Modifié par Daniel Roch
 */
add_filter( 'img_caption_shortcode', 'sxstarter_content_caption', 10, 3 );
function sxstarter_content_caption( $output, $attr, $content ) {
	if ( is_feed() ) {
		return $output;
	}

	$defaults = array(
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	);

	$attr = shortcode_atts( $defaults, $attr );

	// If the width is less than 1 or there is no caption, return the content wrapped between the [caption] tags
	if ( $attr['width'] < 1 || empty( $attr['caption'] ) ) {
		return $content;
	}

	$pattern     = "/(.*?)<img(.*?)height=\"(.*?)\"(.*?)/i";
	$replacement = '$3';
	$height      = preg_replace( $pattern, $replacement, $content );

	$figcaptionclass = "";
	$figureclass     = "";
	if ( $height < 150 ) {
		$figureclass     = " figurebelow";
		$figcaptionclass = " figcaptionbelow";
	}

	$attributes = ( ! empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
	$attributes .= ' class="wp-caption ' . esc_attr( $attr['align'] ) . $figureclass . '"';
	// $attributes .= ' style="width: ' . esc_attr($attr['width']) . 'px"';

	$output = '<figure' . $attributes . ' role="group" itemscope itemtype="http://schema.org/ImageObject">';
	$output .= do_shortcode( $content );

	$output .= '<figcaption class="wp-caption-text' . $figcaptionclass . '" id="wp-caption--' . $attr['id'] . '" itemprop="caption"><span class="figcaptionspan">' . $attr['caption'] . '</span></figcaption>';
	$output .= '</figure>';

	return $output;
}

/**
 * Présentations des commentaires
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function sxstarter_custom_comments( $comment, $args, $depth ) {
	$author = array(
		"email" => get_the_author_meta( 'user_email', get_the_author() ),
		"name"  => get_the_author()
	);

	// Si l'auteur du commentaire est l'auteur de l'article //
	$is_author = ( true === ( $comment->comment_author == $author["name"] || $comment->comment_author_email == $author["email"] ) ) ? 'is-author' : '';

	// Date du commentaire //
	$day  = date_i18n( "d F Y", strtotime( $comment->comment_date ) );
	$time = date_i18n( "G\hi", strtotime( $comment->comment_date ) );
	$date = sprintf( __( 'The %s at %s', 'sxstarter' ), $day, $time ); ?>

	<li class="item <?php echo $is_author; ?>" id="comment-<?php echo $comment->comment_ID; ?>">
		<div class="avatar">
			<?php $avatar_size = ( wp_is_mobile() ) ? 50 : 100; ?>
			<?php echo get_avatar( $comment->comment_author_email, $avatar_size ); ?>
		</div>
		<div class="metas">
			<span class="name"><span><?php echo $comment->comment_author; ?></span></span>
			<span class="date"><?php echo $date; ?></span>
		</div>
		<div class="content"><?php echo apply_filters('comment_text', $comment->comment_content ); ?></div>
	</li>
	<?php
}


/**
 * Allow svg for medias
 * @param $mime_types
 * @return mixed
 */
function sxstarter_add_myme_types( $mime_types ) {
	$mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
	return $mime_types;
}
//add_filter( 'upload_mimes', 'sxstarter_add_myme_types', 1, 1 );

