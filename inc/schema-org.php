<?php
/**
 * Adapter les informations au client
 */
add_action( 'wp_footer', 'sx_add_schema' );
function sx_add_schema() {

	$datas = [];

	$datas["@context"] = "https://schema.org";
	$datas["@type"]    = "LocalBusiness";

	$datas["name"] = get_bloginfo( 'name' );

	$datas["description"] = get_bloginfo( 'description' );

	$datas["url"] = home_url();
	$datas["sameAS"] = [
		"https://www.linkedin.com/company/XXXX",
	];

	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

	$datas["logo"] = $logo[0];
	$datas["image"] = $logo[0];

	$datas["telephone"] = "+33 (0) xxxxx";

	$datas["priceRange"] = "0-50000€";

	$datas["email"] = "mailto:contact@xxxxx.fr";

	$datas["openingHours"] = [
		"Mo,Tu,We,Th,Fr 09:00-18:00"
	];

	$datas['geo'] = [
		"@type" => "GeoCoordinates",
		"latitude" => "xxxxxxxxxxx",
        "longitude" => "xxxxxxxxxxxxxxxxxx"
	];

	$datas['address'] = [
		"@type" => "PostalAddress",
		"streetAddress" => "xxxxxxxxxxxxxxx",
        "addressLocality" => "Paris",
        "addressRegion" => "FR",
        "areaServed" => "EU",
        "postalCode" => "xxxxxxxxxxxx"
	];

	$datas['contactPoint'] = [
		"@type" => "ContactPoint",
		"telephone" => "+33 (0) xxxxx",
		"contactType" => "customer service",
		"hoursAvailable" => "Mo,Tu,We,Th,Fr 09:00-18:00",
		"availableLanguage" => [
			"@type" => "Language",
			"name" => "French"
		]
	];

	echo '<script type="application/ld+json">';
	echo json_encode( $datas );
	echo '</script>';

}