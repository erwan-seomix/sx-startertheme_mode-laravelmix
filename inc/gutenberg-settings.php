<?php
/**
 * Réglages des options spécifiques à Gutenberg
 */
function sxstarter_setup_theme_supported_features() {

	// Definir une palette de couleurs en accord avec la charte graphique du thème
	add_theme_support( 'editor-color-palette',
		array(
			array( 'name' => 'blue', 'slug'  => 'blue', 'color' => '#0e9cb6' ),
			array( 'name' => 'yellow', 'slug'  => 'yellow', 'color' => '#ffcb00' ),
			array( 'name' => 'light pink', 'slug'  => 'light-pink', 'color' => '#f0eded' ),
			array( 'name' => 'white', 'slug'  => 'white', 'color' => '#ffffff' ),
			array( 'name' => 'black', 'slug'  => 'black', 'color' => '#000000' ),
			array( 'name' => 'grey', 'slug'  => 'grey', 'color' => '#6a6a6a' ),
			array( 'name' => 'light grey', 'slug'  => 'light-grey', 'color' => '#F5F4F4' )
		)
	);

	// désactiver la personnalisation des couleurs
	//add_theme_support( 'disable-custom-colors' );
}
//add_action( 'after_setup_theme', 'sxstarter_setup_theme_supported_features' );

// see classes .has-slug-color and .has-slug-background-color in scss/base/_override-gut-style.scss