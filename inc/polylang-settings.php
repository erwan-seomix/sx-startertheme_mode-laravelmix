<?php
/**
 * Add CPT Private for translations
 * @see https://polylang.pro/doc/filter-reference/#pll_get_post_types
 */
add_filter( 'pll_get_post_types', 'add_cpt_to_pll', 10, 2 );

function add_cpt_to_pll( $post_types, $is_settings ) {
	if ( $is_settings ) {
		// enables language and translation management for 'my_cpt'
		$post_types['mon_cpt'] = 'mon_cpt';
	}
	return $post_types;
}
