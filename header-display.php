<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'sxstarter' ); ?></a>

    <header class="header-main" id="header" role="banner">
        <div class="wrap">
            <div id="header-one" class="site-branding">
				<?php
				// NB: on peut faire apparaitre ou disparaitre le nom du site et description via la customizer
				// Si le logo est renseigné, on l'active.
				if ( function_exists( 'get_custom_logo' ) && ! false == get_custom_logo() ) :
					// Logo.
					the_custom_logo();
				endif;

				// Container H1 sur le nom de site pour la page d'accueil sinon DIV.
				$container_site_title = ( is_front_page() && is_home() || is_front_page() ) ? 'h1' : 'div';
				?>
                <<?php echo $container_site_title;?> id="site-title" class="site-title" >
				<?php
				// Mettre un lien sur le nom du site si on n'est pas sur la page d'accueil.
				if ( $container_site_title == 'div' ) {
					echo '<a href="' . home_url() . '">';
				}

				// Le nom du site.
				echo esc_attr( get_bloginfo( 'name', 'display' ) );

				// Fermer mettre un lien sur le nom du site.
				if ( $container_site_title == 'div') {
					echo '</a>';
				}
				?>
            </<?php echo $container_site_title; ?> >

			<?php
			// Si la description est remplie, on l'affiche.
			if ( ! empty( get_bloginfo( 'description', 'display' ) ) ) :
				$sxchild_description = get_bloginfo( 'description', 'display' );
				if ( $sxchild_description || is_customize_preview() ) : ?>
                    <p id="site-description" class="site-description"><?php echo $sxchild_description; /* WPCS: xss ok. */ ?></p>
				<?php
				endif;
			endif;
			?>
        </div>


        <div id="header-second">

            <!-- Nav principale -->
            <button type="button" id="burger-menu" aria-controls="primary-menu" aria-expanded="false" aria-label="<?php esc_attr_e( 'Main menu', 'sxstarter' ); ?>" class="menu-link"><span class="menu-bar"></span><span class="screen-reader-text"><?php esc_html_e( 'Dropdown Menu', 'sxstarter' ); ?></span></button>
            <nav role="navigation" id="header-navmenu" class="site-navigation" itemscope itemtype="http://schema.org/SiteNavigationElement" aria-label='<?php esc_attr_e( 'Primary Menu ', 'europebs' ); ?>'>

                <?php

				wp_nav_menu(
					array(
						'container'      => 'true',                                     // remove nav container
						// 'container_class' => 'menu cf',                              // class of container (should you choose to use it)
						'menu'           => __( 'Menu du haut de page', 'sxstarter' ), // nav name
						'menu_class'     => 'menu-items',                              // adding custom nav class
						'theme_location' => 'primary_menu',                        // where it's located in the theme
						'before'         => '',                                         // before the menu
						'after'          => '',                                         // after the menu
						'link_before'    => '',                                         // before each link
						'link_after'     => '',                                         // after each link
						'depth'          => 3,                                          // limit the depth of the nav
						'fallback_cb'    => '',                                         // fallback function (if there is one)
						// 'walker' => new sxstarter_content_menu_walker()                 // Custom Walker if necessary
					)
				);
				?>
            </nav>
        </div>
</div>
</header>

<div id="content" class="site-content">
    <div class="breadcrumb-area">
        <div class="wrapper">
			<?php
			// Breadcrumbs.
			if ( function_exists( 'sxstarter_breadcrumb_add' ) && ! is_front_page() ) :
				sxstarter_breadcrumb_add();
			endif;
			?>
        </div>
    </div>
