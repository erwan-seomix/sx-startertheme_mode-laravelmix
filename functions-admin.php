<?php 


//************************************** I - Médias
/**
 * Ajout de la balise title au lien lors de l'ajout d'une image
 * Utile notamment pour la Fancybox
 * Par Maximilian http://blog.maximusbusiness.com/2012/12/adding-image-title-in-image-link-title-attribute/ 
 */
add_filter( 'image_send_to_editor', 'seomix_attachment_with_title', 10, 3 );
function seomix_attachment_with_title( $html, $id, $attachment ) {
	// First, we grab the title from the passed $id.
	$title = get_the_title( $id );
	// First we check if there is a link with a preg_match.
	preg_match( "/<a[^>]*>/", $html, $matches );
	// Then we add it to the link with a str_replace and add it back to $html.
	$html = str_replace( "<a ", "<a title=\"$title\" ", $html );
	// return $html .
	return $html;
}

/**
 * Pas de perte de qualité pour les images (on laisse les extensions de compression d'image faire le job)
 */
add_filter( 'jpeg_quality', 'seomix_image_quality' );
add_filter( 'wp_editor_set_quality', 'seomix_image_quality' );
function seomix_image_quality( $quality ) {
	return 100;
}




//************************************** II - Administration des utilisateurs
/**
 * Pour les administrateurs
 */
if ( current_user_can( 'activate_plugins' ) ) {
	/**
	 * Fonction de tri des articles par auteur (par boiteaweb)
	 *
	 */
	function seomix_adm_editmenu_post_filter_author() {
		global $wpdb, $typenow;
		// On prepare la requete pour recuperer tous les auteurs qui ont publiés au moins 1 article.
		$query = $wpdb->prepare( 'SELECT DISTINCT post_author FROM ' . $wpdb->posts . ' WHERE post_type = %s', $typenow );
		// On recupere les id.
		$users = $wpdb->get_col( $query );
		// On génére le select avec la liste des auteurs.
		wp_dropdown_users( array(
			'show_option_all'  => 'Voir tous les auteurs',
			'show_option_none' => false,
			'name'             => 'author',
			'include'          => $users,
			'selected'         => ! empty( $_GET['author'] ) ? (int)$_GET['author'] : 0,
			'include_selected' => true,
		));
	}
	add_action( 'restrict_manage_posts', 'seomix_adm_editmenu_post_filter_author' );


/**
 *Pour les non administrateurs
 */
} else {
	/**
	 * Ajout de capacités à certains rôles.
	 */
	function seomix_adm_user_rolecaps_add() {
		global $wp_roles;
		// Rôle concerné.
		$role = get_role( 'contributor' );
		// Ajout d'une action.
		$role->add_cap( 'upload_files' );
	};
	add_action( 'admin_head', 'seomix_adm_user_rolecaps_add' );


	/**
	 * Ajout à un éditeur la capacité d'éditer les options d'un thème.
	 */
	add_action( 'admin_init', 'customize_meta_boxes' );
	function customize_meta_boxes() {
		if ( ! current_user_can( 'administrator' ) ) {
			$role = get_role( 'editor' );
			$role->add_cap( 'edit_theme_options' );
			$role->remove_cap( 'switch_themes' );
			$role->remove_cap( 'install_themes' );
			$role->remove_cap( 'edit_themes' );
		}
	}


	/**
	 * Ne montrer que ses posts à l'utilisateur
	 */
	function seomix_adm_user_show_myposts( $query ) {
		if ( ! current_user_can( 'edit_pages' ) ) {
			global $user_ID;
			$query->set( 'author', $user_ID );
			unset( $user_ID );
			$screen = get_current_screen();
			add_filter( 'views_' . $screen->id, 'seomix_adm_user_remove_post_counts' );
		}
		return $query;
	}
	function seomix_adm_user_remove_post_counts( $views ) {
		//$views = array_intersect_key($views, array_flip(array('mine','trash')));
		$views = array_intersect_key( $views, array_flip( array( 'mine' ) ) );
		//unset($views['all']);
		return $views;
	}
	add_filter( 'pre_get_posts', 'seomix_adm_user_show_myposts' );


	/**
	 * Restreindre l'accès de la bibliothèque d'image à ses propres images uniquement
	 */
	function seomix_adm_user_media_acces( $wp_query ) {
		$screen = get_current_screen();
		if ( 'upload' == $screen->base ) {
			if ( ! current_user_can( 'moderate_comments' ) ) {
				global $current_user;
				$wp_query->set( 'author', $current_user->id );
			}
		}
	}
	add_filter( 'parse_query', 'seomix_adm_user_media_acces' );


	/**
	 * Masquer les notifications de mise à jour de WordPress
	 */
	if ( ! current_user_can( 'update_plugins' ) ) {
		add_action( 'admin_init', create_function( false, "remove_action( 'admin_notices', 'update_nag', 3 );" ) );
	}
} // Fin du bloc pour les non administrateurs 






//************************************** III - Interface admin
/**
 * FOOTER : Changer le texte gauche du Footer
 *
 */
function seomix_adm_footer_text_left( $text ) {
	$text .= ' Et oui : WordPress, c\'est quand même de la bombe, non ?';
	return $text;
}
add_filter( 'admin_footer_text', 'seomix_adm_footer_text_left' );

/**
 * FOOTER : Changer le texte droit du Footer
 *
 */
function seomix_adm_footer_text_right( $text ) {
	$textbefore = 'WordPress ';
	// Afficher les données du thème.
	$theme_data = wp_get_theme();
	$text      .= ' - Thème <strong>' . $theme_data['Name'] . '</strong> Version ' . $theme_data['Version'] . ' par <strong><a href="https://www.seomix.fr">SeoMix</a></strong>';
	$textfinal  = $textbefore . $text;
	return $textfinal;}
add_filter( 'update_footer', 'seomix_adm_footer_text_right', 11 );







//************************************** V - Contenus

/**
 * Changer le titre par défaut lors de la création d'un article
 *
 */
function seomix_adm_default_post_title( $title ) {
	$screen = get_current_screen();

	if ( 'post' == $screen->post_type )
		$title = 'Titre de votre article';
	return $title;
}
//add_filter( 'enter_title_here', 'seomix_adm_default_post_title' );

/**
 * Autoriser l'HTML dans les descriptions des utilisateurs
 *
 */
remove_filter( 'pre_user_description', 'wp_filter_kses' );
add_filter( 'pre_user_description', 'wp_filter_post_kses' );
