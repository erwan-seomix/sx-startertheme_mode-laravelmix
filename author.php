<?php
get_header(); ?>

<div class="container">
    <main id="main" class="content-area primary" role="main">
        <div class="content">
            <header class="page-header">
                <h1><?php the_author_meta( 'display_name' ); ?></h1>
            </header>
            <?php
            echo get_avatar( get_the_author_meta( 'email' ), '100' , '', get_the_author_meta( 'display_name' ), array( "class" => 'alignleft' ) );
            echo the_author_meta( 'description' );
            ?>

            <div class="page-content">
                <h2><?php esc_html_e('The lasts posts by ', 'sxstarter'); ?><?php the_author_meta( 'display_name' ); ?></h2>
                <?php
                while ( have_posts() ) :
                    the_post();
                    ?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'alignleft' ) ); ?>
                    <?php the_excerpt();?>
                    <?php
                endwhile;
                ?>
            </div>
        </div>
    </main>
    <?php
    // Sidebar.
    get_sidebar();?>
</div>

<?php
// Afficher le footer.
get_footer();
