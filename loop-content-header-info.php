<?php

// Auteur.
echo '<span class="infos-content infos-author">';
	esc_html_e( 'by ', 'sxstarter' );
	the_author();
echo '</span>';

// Date de publication.
echo '<span class="infos-content infos-date">';
	the_date();
echo '</span>';

// Pour les catégories, on affiche le nom de la catégorie.
if ( is_category() ) {
	// On montre juste le nom de la catégorie.
	$categories = get_the_category();
	if ( ! empty( $categories ) ) {
		echo '<span class="infos-content infos-category">';
				echo esc_html( $categories[0]->name );
		echo '</span>';
	}
} elseif ( is_single() || is_search() || is_front_page() || is_home() ) { // Pour les articles, on affiche le lien vers la catégorie.
	echo '<span class="infos-content infos-category">';
			the_category( ' ', 'multiple' );
	echo '</span>';
} elseif ( is_singular() && ! is_page() ) { // Affichage d'une taxonomy pour les autres post Types.
		echo '<span class="infos-content infos-taxonomy">';
			$taxonomy_names = get_object_taxonomies( $post );
			echo get_the_term_list( $post->ID, $taxonomy_names[0], '', '', '' );
		echo '</span>';
}
