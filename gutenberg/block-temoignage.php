<?php
/**
 * Block Name: Temoignage
 *
 * Template part that displays a testimonial block created with ACF.
 */
// Get the block id and make it an html ID attribute for styling purposes
$id = 'temoignage-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'temoignage';
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
}

// Load values and assign defaults.
$visuel_id = get_field('temoignage_visuel')?: 122; // id image default !
$nom = get_field('temoignage_nom');
$prenom = get_field('temoignage_prenom');
$fonction = get_field('temoignage_fonction');
$citation = get_field('temoignage_citation');
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?> custom-block">

	<div class="temoignage-image-container">
		<div class="temoignage-image">
			<?php echo wp_get_attachment_image( $visuel_id, 'full' ); ?>
		</div>
	</div>

	<blockquote class="temoignage-blockquote">
		<div class="temoignage-header">
			<p class="temoignage-auteur"><?php echo $prenom . ' ' . $nom; ?></p>
			<p class="temoignage-fonction"><?php echo $fonction; ?></p>
		</div>
		<p class="temoignage-content"><?php echo $citation; ?></p>
	</blockquote>
</div>