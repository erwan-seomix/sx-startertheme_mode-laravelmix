<?php
/**
 * Block Name: R1-Bar-progress
 *
 * Template part that displays a testimonial block created with ACF.
 */
// Get the block id and make it an html ID attribute for styling purposes
$id = 'bar-progress-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'bar-progress';
//if( !empty($block['className']) ) {
//	$className .= ' ' . $block['className'];
//}
//if( !empty($block['align']) ) {
//	$className .= ' align' . $block['align'];
//}

// Check rows exists.
if (have_rows('progress-container')):

	// Loop through rows.
	while (have_rows('progress-container')) : the_row();

		// Load sub field value.
		$percent = get_sub_field('percent');
		$barColor = get_sub_field('barcolor');
		$skillName = get_sub_field('skillname');
		// Do something...

		?>

<h2><?= $skillName ?></h2>

	<?php


		// End loop.
	endwhile;

// No value.
else :
	// Do something...
	echo 'toto';
endif;

?>
<script>

</script>
