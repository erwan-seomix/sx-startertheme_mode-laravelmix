<?php
get_header();?>

<div class="container">
    <main id="main" class="content-area primary" role="main">

        <header class="page-header">
            <div class="wrapper">
                <div class="header-content">
                    <?php
                    echo '<h1>'. esc_html_e('Page not found', 'sxstarter' ).'</h1>';
                    ?>
                </div>
            </div>
        </header>
        <div class="entry-content">
            <?php
            echo '<p>'. esc_html__('Page you were looking for is not found', 'sxstarter' ).'</p>';

            // Check if there is useful data or no
            if ( function_exists( 'sxstarter_content_404suggest') ) {
                sxstarter_content_404suggest();
            }
            if ( have_posts() ) :

                echo '<p>'. esc_html__('Perhaps you were looking for one of these contents :', 'sxstarter' ).'</p>';

                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'loop', 'listing' );
                endwhile;

            else :

                echo '<p>'. esc_html_e('Do a search to find the content you are interested in:', 'sxstarter' ).'</p>';
                get_search_form();

            endif; ?>
        </div>

	</main>

	<?php
	get_sidebar();?>
</div>

<?php
get_footer();