<?php

// Afficher le header.
get_header();?>

<div class="container">
    <main id="main" class="content-area primary" role="main">

        <header class="page-header">
            <div class="wrapper">
                <div class="header-content">
                    <h1><?php _e( 'Your search', 'sxstarter' ); ?></h1>
                    <?php global $wp_query;?>
                    <p><?php echo $wp_query->found_posts; ?> <?php _e( 'result(s) for', 'sxstarter' ); ?>: "<?php the_search_query(); ?>"</p>
                </div>
            </div>
        </header>

        <?php
        if ( have_posts() ) :?>
            <div class="posts-wrapper">
                <?php
                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'loop', 'listing' );
                endwhile;
                ?>
            </div>
            <div class="sx-posts-navigation">
                <?php
                // pagination personnalisée voir dans /inc/custom-functions.php
                sxstarter_pagination(); ?>
            </div>
            <?php
        else: ?>
            <div class="wrapper">
                <div class="no-result">
                    <p class><?php esc_html_e('Sorry, but we haven\'t found anything that matches your search for', 'sxstarter'); ?> : "<span class="bold"><?php the_search_query(); ?>"</span></p>
                    <p><?php esc_html_e('Try again with different keywords', 'sxstarter'); ?> :</p>
                    <?php get_search_form(); ?>
                </div>
            </div>
            <?php
        endif;
        ?>

    </main>

    <?php
    // Sidebar.
    get_sidebar();?>
</div>

<?php
// Afficher le footer.
get_footer();
