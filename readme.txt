=== SX Starter ===
Contributors: the Seomix team
Tested up to: 5.5.1
Stable tag: 1.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Starter theme

== Changelog ==

= 1.0 =
* Released: October 15, 2020

Initial release