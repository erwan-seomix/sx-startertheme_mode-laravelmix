<article <?php post_class('listing'); ?>>
	<header>
		<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2><?php ?></a>
        <?php
		// Données du contenu.
		get_template_part( 'loop', 'content-header-info' );?>
	</header>
	<?php
	// Thumbnail.
	the_post_thumbnail( 'thumbnail', array( 'class' => 'alignleft' ) );

	// Le contenu.
	the_excerpt();
	?>
</article>
