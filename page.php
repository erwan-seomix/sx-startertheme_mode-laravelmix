<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();?>

<div class="container">
    <main id="main" class="content-area primary" role="main">

        <?php
        if ( have_posts() ) :
            while ( have_posts() ) :
                the_post();
	            get_template_part( 'template-parts/content', get_post_type() );
            endwhile;
        endif;
        ?>

    </main>

    <?php
    // Sidebar.
//    get_sidebar();?>
</div>

<?php
// Afficher le footer.
get_footer();
