<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="searchbar-container">
		<label for="search-form-input" class="screen-reader-text"><?php echo esc_attr_x( 'Search for a post', 'placeholder', 'sxstarter' ); ?></label>
			<input type="search" id="search-form-input" class="search-field"
				   placeholder="<?php echo esc_attr_x( 'Search for a post', 'placeholder', 'sxstarter' ); ?>"
				   value="<?php echo get_search_query(); ?>" name="s"
				   title="<?php echo esc_attr_x('Search for a post', 'label', 'sxstarter' ); ?>" />
		<button type="submit" class="search-submit" aria-label="<?php esc_attr_e('Submit your keyword for search', 'sxstarter'); ?>"></button>
	</div>
</form>
