	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
        <div class="wrap">
            <p class="source-org copyright"><?php if ( function_exists( 'sxstarter_footer_copyright_date' ) ) {
                sxstarter_footer_copyright_date();
            }?> <?php bloginfo( 'name' ); ?>.</p>
        </div>
        <button type="button" aria-label="<?php esc_attr_e( 'Haut de page', 'sxstarter' ); ?>" id="button-backtotop" title="<?php esc_attr_e( 'Retourner en haut de page', 'sxstarter' ); ?>"></button>
	</footer>

</div> <!-- / Container -->

<?php 
	// Fonction indispensable pour WordPress.
	wp_footer();
?>

</body>
</html>
