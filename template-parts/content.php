<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<h1><?php the_title(); ?></h1>
		<?php get_template_part( 'loop', 'content-header-info' ); ?>
	</header>

	<div class="entry-content">
	<?php
		// Le contenu
		// Pas de duplication de contenu sur les pages de commentaires.
		$cpage = get_query_var( 'cpage' );
		if ( is_singular() && $cpage > 0 ) {
			echo "You are on a comment page. Here is an excerpt from the article : ";
			the_excerpt();
		} else {
			the_content();
		}

		// Pas de commentaires si page attachment.
		if ( 'attachment' != get_post_type( get_the_ID() ) ) {
			comments_template( '', true );
		}
	?>
	</div>
</article>