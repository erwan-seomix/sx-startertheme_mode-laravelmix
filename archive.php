<?php
get_header();?>

<div class="container">
    <main id="main" class="content-area primary" role="main">
        <header class="page-header">
            <div class="wrapper">
                <div class="header-content">
                    <h1><?php the_archive_title(); ?></h1>
    <!--                <h1>--><?php //single_term_title(); ?><!--</h1>-->
                    <p><?php the_archive_description(); ?></p>
    <!--                <p>--><?php //term_description(); ?><!--</p>-->
                </div>
            </div>
        </header>


        <?php
        if ( have_posts() ) :?>
            <div class="posts-wrapper">
                <?php
                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'loop', 'listing' );
                endwhile; ?>

                <div class="sx-posts-navigation">
                    <?php
                    // pagination personnalisée voir dans /inc/custom-functions.php
                    sxstarter_pagination(); ?>
                </div>

            </div>
                <?php
        endif; ?>

    </main>

    <?php
    get_sidebar(); ?>
    </div>
<?php
get_footer();