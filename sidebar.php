<aside id="sidebar" class="aside-area secondary">

	<?php
	// Recherche.
	echo '<p class="screen-reader-text widget-title">Recherche</p>';
	get_search_form();
	?>

	<?php
	// Sidebar.
	if ( is_active_sidebar( 'sidebar-1' ) ) {
		dynamic_sidebar( 'sidebar-1' );
	}
	?>

</aside>
