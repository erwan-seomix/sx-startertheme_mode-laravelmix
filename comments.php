<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
*/
if ( post_password_required() ) {
	return;
}
?>

<div itemprop="discussionUrl" id="comments" class="comments-area">
	<?php
	if ( have_comments() ) :

		// Variables.
		$author = array(
			'highlight' => 'highlight',
			'email'     => get_the_author_meta( 'user_email', get_the_author() ),
			'name'      => get_the_author(),
		);
		?>

		<ol class="comment-list">
			<?php
				wp_list_comments(
					array(
						'callback'    => 'sxstarter_custom_comments', // See inc/custom-functions.php
						'type'        => 'comment',
						'reply_text'  => '',
						'format'      => 'html5',
						'echo'        => true,
						'style'      => 'ol',
					)
				);
			?>
		</ol>

		<?php
	else :
		?>
		<div class="h2diff"><?php esc_html_e('No comments yet', 'sxstarter'); ?></div>
		<?php
	endif;

	if ( comments_open() ) :
		$required_text = '';
		$aria_req      = '';
		$args          = array(
			'id_form'              => 'commentform',
			'id_submit'            => 'submit',
			'title_reply'          => __( 'Post a comment', 'sxstarter' ),
			'title_reply_to'       => __( 'Reply to %s', 'sxstarter' ),
			'cancel_reply_link'    => __( 'Cancel the answer', 'sxstarter' ),
			'label_submit'         => __( 'Submit Comment', 'sxstarter' ),
			'comment_field'        => '
				<p class="comment-form-comment">
					<label for="comment">' . esc_html_e( 'Enter your comment below', 'sxstarter' ) . '</label>
					<textarea id="comment" name="comment" rows="8" required aria-required="true"></textarea>
				</p>',
			'must_log_in'          => '
				<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'sxstarter' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
			'logged_in_as'         => '
				<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'sxstarter' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
			'comment_notes_before' => '',
			// 'comment_notes_after' => '<p class="urlsmall">Pour ajouter du code, utilisez "&lt;code&gt;"</p>',
			'comment_notes_after'  => '',
			'fields'               => apply_filters( 'comment_form_default_fields', array(
				'author' => '
						<p class="comment-form-author">' . '
							<label for="author">' . __( 'Your name', 'sxstarter' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label><input id="author" placeholder="' . __( 'Enter a real name', 'sxstarter' ) . '" required aria-required="true" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />
						</p>',
				'email'  => '
					<p class="comment-form-email">
						<label for="email">' . __( 'Your email', 'sxstarter' ) . ( $req ? '<span class="required">*</span>' : '' ) . '</label><input pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" placeholder="' . __( 'will never be published', 'sxstarter' ) . '" required aria-required="true" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />
					</p>',
				'url'    => '
					<p class="comment-form-url">
						<label for="url">' . __( 'Your web site web', 'sxstarter' ) . '</label>' . '<input pattern="https?://.+" id="url" placeholder="L\'adresse de votre site personnel" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />
					</p>',
			))
		);
		comment_form( $args );
		?>
		<!--commentaires fermés-->
		<?php
	else :
		?>
		<p>
			<?php
		_e ( 'Comments are closed. If you want to add an important element to the conversation, please use the contact form.', 'sxstarter' );
		?></p>
		<?php
	endif;
	?>
</div>
