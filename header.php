<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!--><html <?php language_attributes(); ?>><!--<![endif]-->
<head>
	<?php
		// Ajoutés via hook wp_head()
		//   -> sxstarter_head_content_base   => Code HTML de base
		//   -> sxstarter_head_content_favicons  => Favicons
		wp_head();
	?>
</head>
<body <?php body_class(); ?>>
	<?php
		// trigger action for anyone that need to add code just after <body>.
		wp_body_open();
		// Charger l'affichage du haut des pages.
		get_template_part( 'header-display' );
	?>
