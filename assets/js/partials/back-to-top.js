jQuery(document).ready(function($) {

  mybutton = document.getElementById("button-backtotop");

  window.onscroll = function() {scrollFunction()};

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  }

  $("#button-backtotop").click(function() {
    $("html, body").animate({ scrollTop: 0 }, 1000);
    return false;
  });
});