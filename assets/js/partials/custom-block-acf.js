// JS pour les blocks en Front, dupliqué dans src/assets/js/gutenberg pour le BO (en front js concaténé dans main.js du theme via Gulp)
// Exemple utilisation de Slick https://kenwheeler.github.io/slick/
// Pour realiser des sliders dans des blocks gutenberg créées avec ACF
// Le JS du block doit être ajouté en enqueue_assets dans l'enregistrement du block xxx-slider
// https://www.advancedcustomfields.com/blog/building-a-custom-slider-block-in-30-minutes-with-acf/

(function($) {

  // /**
  //  * initializeBlock for xxx-slider
  //  *
  //  * Adds custom JavaScript to the block HTML.
  //  *
  //  * @date    15/4/19
  //  * @since   1.0.0
  //  *
  //  * @param   object $block The block jQuery element.
  //  * @param   object attributes The block attributes (only available when editing).
  //  * @return  void
  //  */
  // var initializeBlock = function( $block ) {
  //   $block.find('.xxx-slides').slick({
  //     infinite: true,
  //     dots: true,
  //     arrows: false,
  //     slidesToShow: 1,
  //     mobileFirst: true,
  //     responsive: [
  //       {
  //         breakpoint: 599,
  //         settings: "unslick"
  //       }
  //     ]
  //   });
  // }
  //
  // // Initialize each block on page load (front end).
  // $(document).ready(function(){
  //   $('.xxx').each(function(){
  //     initializeBlock( $(this) );
  //   });
  // });
  //
  // $(window).resize(function(){
  //   $('.xxx').each(function(){
  //     if( $(window).width() < 600 ) {
  //       initializeBlock( $(this) );
  //     }
  //   });
  // });
  //
  // // Initialize dynamic block preview (editor).
  // if( window.acf ) {
  //   window.acf.addAction( 'render_block_preview/type=xxx', initializeBlock );
  // }


//-------------------------
})(jQuery);
