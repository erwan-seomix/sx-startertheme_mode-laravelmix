/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
/**
 * Add a dropdown menu in main navigation aria compatible
 * global screenReaderText : See functions.php
 * See https://github.com/WordPress/WordPress/blob/master/wp-content/themes/twentyfifteen/js/functions.js
 */
module.exports = {

    init: function () {
        let $ = jQuery;
        // do something here
        //alert('example 2 is working...');
        $('#burger-menu').on('click', function () {
            $('#header-navmenu').slideToggle(600);
            $(this).toggleClass('active');
            console.log('pouf');
        })
    }

};
