<?php
/**
 * SxStarter functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sxstarter
 */

/**
 * Globales utilisables partout
 */
// URLS du thème.
define( 'get_template_directory_uri()', get_template_directory_uri() );
// Version du thème
define( 'SX_THEME_VERSION', wp_get_theme()->get( 'Version' ) );

/**
 * Les fonctions de l'administration seront chargées dans un fichier spécifique
 */
if ( is_admin() || is_customize_preview() ) {
	get_template_part( 'functions-admin' );
}


if ( ! function_exists( 'sxstarter_setup' ) ) :
	/**
	 * Activation des fonctionnalités du thème
	 */
	function sxstarter_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Twenty, use a find and replace
		 * to change 'sxstarter' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sxstarter' );
//		load_theme_textdomain( 'sxstarter', get_template_directory_uri() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		//add_theme_support( 'automatic-feed-links' );

		// Set content-width.
		global $content_width;
		if ( ! isset( $content_width ) ) {
			$content_width = 1200;
		}

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// Enable support for Post Thumbnails on posts and pages.
		add_theme_support( 'post-thumbnails' );

		// Set post thumbnail size.
		//set_post_thumbnail_size( 1200, 9999 );

		// Ajout d'une taille d'image si besoin est
		//add_image_size( 'name', 'width', 'height', true );

		/*
		 * Add theme support for Custom Logo.
		 */
		$logo_width  = 120;
		$logo_height = 90;

		// If the retina setting is active, double the recommended width and height.
		if ( get_theme_mod( 'retina_logo', false ) ) {
			$logo_width  = floor( $logo_width * 2 );
			$logo_height = floor( $logo_height * 2 );
		}
		add_theme_support( 'custom-logo', array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );


		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'comment-list',
			'comment-form',
			'search-form',
			'gallery',
			'caption',
			'style',
			'script'
		) );


		/*
		 * Add menus
		 */
//		add_theme_support( 'menus' );

		// Add Excerpts for pages
		//add_post_type_support( 'page', 'excerpt' );

		// Add post formats for post
		//add_theme_support( 'post-formats', array( 'aside' ) ); // 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'

		// Add post format for page
		//add_post_type_support( 'page', 'post-formats' );

		// Add post format for CPT
		// https://wordpress.org/support/article/post-formats/#adding-post-type-support
		//add_post_type_support( 'my_custom_post_type', 'post-formats' );

		// Set local language pour les dates
		$locale = get_locale();
		setlocale( LC_ALL, $locale . '.utf8', $locale );

		/**
		 * Gutenberg Editor
		 */
		// Add support for full and wide align images (Gutenberg).
		add_theme_support( 'align-wide' );

		// Ajouter une feuille de style pour l’éditeur dans l’admin.
		add_theme_support( 'editor-styles' );
		add_editor_style( 'assets/css/style-editor.css' );

		// Load default block styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'sxstarter_setup' );

/**
 * Réglages spécifiques pour l'utilisation de l'éditeur Gutenberg
 */
//get_template_part( 'inc/gutenberg-settings' );


/**
 * Enqueue scripts and styles.
 */
function sxstarter_enqueue_scripts() {

	/*
	 * style.css à la racine du theme nécessaire uniquement à la déclaration du thème pour WP
	 * ici vide de style, pas d'obligation de le rendre accessible en front
	 */
	//wp_enqueue_style( 'sxstarter-style', get_stylesheet_uri(), array(), SX_THEME_VERSION );

	/* Feuille CSS Principale Générée via Gulp*/
	wp_register_style( 'sxstarter-mainstyle', get_template_directory_uri() . '/build/css/app.css', array(), SX_THEME_VERSION );
	wp_enqueue_style( 'sxstarter-mainstyle' );

	// Ajout des Dashicons
	wp_enqueue_style  ( 'dashicons' );

	/* JS Généré via Gulp*/
	wp_enqueue_script( 'sxstarter_scripts', get_template_directory_uri() . '/build/js/app.js', array( 'jquery' ), SX_THEME_VERSION, true );
	//wp_enqueue_script( 'sxstarter_scripts', get_template_directory_uri() . '/assets/js/dev-app.js', array( 'jquery' ), SX_THEME_VERSION, true );
	//wp_enqueue_script( 'sxstarter_scripts', get_template_directory_uri() . '/assets/js/dev-vendor.js', array( 'jquery' ), SX_THEME_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Make ScreenReaderText translatable accessible in scripts.
//	wp_localize_script(
//		'sx_child_scripts',
//		'screenReaderText',
//		array(
//			'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'sxstarter' ) . '</span>',
//			'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'sxstarter' ) . '</span>',
//		)
//	);
}
add_action( 'wp_enqueue_scripts', 'sxstarter_enqueue_scripts', 11 );


/**
 * Préchargement des typos
 *
 * @since  1.0
 * @author  Daniel Roch
 *
 * @hook wp_head, 21
 */
function sxstarter_preload_font(){

	$render = '<link rel="preload" as="font" href="' . get_template_directory_uri() . '/build/fonts/roboto-v20-latin-regular.woff2" type="font/woff2" crossorigin="anonymous">';
	$render .= '<link rel="preload" as="font" href="' . get_template_directory_uri() . '/build/fonts/roboto-v20-latin-regular.woff" type="font/woff" crossorigin="anonymous">';

// A décommenter quand des icones seront générées
//	$render .= '<link rel="preload" as="font" href="' . get_template_directory_uri() . '/assets/icons/font/iconsfont.woff2" type="font/woff2" crossorigin="anonymous">';
//	$render .= '<link rel="preload" as="font" href="' . get_template_directory_uri() . '/assets/icons/font/iconsfont.woff" type="font/woff" crossorigin="anonymous">';

	echo $render;
}
add_action('wp_head', 'sxstarter_preload_font', 21);

/**
 * Register navigation menus uses wp_nav_menu in three places.
 */
function sxstarter_theme_menus() {
	register_nav_menus(
		array(
			'primary_menu' => __( 'Primary Menu', 'sxstarter' ),
			'footer_menu' => __( 'Footer Menu', 'sxstarter' )
		)
	);
}
add_action( 'after_setup_theme', 'sxstarter_theme_menus' );

/**
 * BreadCrumbs
 */
get_template_part( 'inc/breadcrumb/functions-breadcrumb' );
function sxstarter_breadcrumb_add() {
	get_template_part( 'inc/breadcrumb/breadcrumb' );
}
// Modification du séparateur dans le chemin de navigation.
function sxstarter_breadcrumb_separator() {
	return ' <span class="breadcrumb-separator" aria-hidden="true">></span> ';
}
add_filter( 'mash_breadcrumb_separator', 'sxstarter_breadcrumb_separator' );


/**
 * Zones de widgets
 */
function sxstarter_sidebar_init() {
	register_sidebar(
		array(
			'name'          => __( 'Footer', 'sxstarter' ),
			'id'            => 'widgets-footer',
			'description'   => __( 'Add widgets here to appear in your footer.', 'sxstarter' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<p class="widget-title">',
			'after_title'   => '</p>',
		)
	);
//	 register_sidebar(
//	 	array(
//		 'name' => __( 'Sidebar Gauche', 'sxstarter' ),
//		 'id' => 'sidebar-left',
//		 'description' => __( 'Sidebar ', 'sxstarter' ),
//		 'before_widget' => '<section id="%1$s" class="widget %2$s">',
//		 'after_widget' => '</section>',
//		 'before_title' => '<header>',
//		 'after_title' => '</header>',
//	    )
//	 );
}
add_action( 'widgets_init', 'sxstarter_sidebar_init' );

/**
 * Fonctions déportées pour faciliter leur rangement
 */

// Customizer.
get_template_part( 'inc/customizer' );

// Hooks, Filtres divers
get_template_part( 'inc/custom-functions' );

// Règles additionnelles pour GravityForm.
//get_template_part( 'inc/gravityform-settings' );

// schema.org
// get_template_part( 'inc/schema-org' );

// Polylang fonctions additionnelles
//get_template_part( 'inc/polylang-settings' );

// Google-tag-manager
//get_template_part( 'inc/google-tag-manager' );
